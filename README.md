# course_evaluation

## Features
- Select course
- Select category
- Rate course

---
## Contributors
* Jasmine McKenzie <jmckenzie@claflin.edu>
* Detravious Brinkley <detraviousbrinkley@gmail.com>

---
## Run Code
1. Clone Repo
2. Open xcode
3. Unzip "course_evaluation"

## License & copyright
Contributors
